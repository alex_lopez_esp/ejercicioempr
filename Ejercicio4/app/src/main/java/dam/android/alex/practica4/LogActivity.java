package dam.android.alex.practica4;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;

import java.sql.Array;
import java.text.DecimalFormat;

public class LogActivity extends AppCompatActivity {
    private  static final String DEBUG_TAG="LogsAndroid-1";



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.i(DEBUG_TAG, "LOG-onCreate"+getLocalClassName());
        notify("LOG-onCreate");

    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.i(DEBUG_TAG, "LOG-onStart"+getLocalClassName());

        notify("LOG-onStart");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.i(DEBUG_TAG, "LOG-onStop"+getLocalClassName());
        notify("LOG-onStop");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.i(DEBUG_TAG, "LOG-onPause"+getLocalClassName());
        notify("LOG-onPause");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.i(DEBUG_TAG, "LOG-onResume"+getLocalClassName());
        notify("LOG-onResume");
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        Log.i(DEBUG_TAG, "LOG-onRestart"+getLocalClassName());
        notify("LOG-onRestart");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (isFinishing()){
            Log.i(DEBUG_TAG, "El usuario lo ha eliminado "+getLocalClassName());
        }else {
            Log.i(DEBUG_TAG, "El System lo ha eliminado "+getLocalClassName());
        }
        notify("LOG-onDestroy ");

    }

    @Override
    protected void onSaveInstanceState( Bundle outState) {
        super.onSaveInstanceState(outState);
        Log.i(DEBUG_TAG,"on save InstanceState");
        notify("on save InstanceState");
    }

    @Override
    protected void onRestoreInstanceState( Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        Log.i(DEBUG_TAG,"on restore InstanceState");
        notify("on restore InstanceState");

    }



    private void notify(String eventName){
        String activityName=this.getClass().getSimpleName();
        String CHANNEL_ID="my_LifeCycle";
        if (Build.VERSION.SDK_INT>=Build.VERSION_CODES.O){
            NotificationChannel notificationChannel=new NotificationChannel(CHANNEL_ID,"My Lifecycle", NotificationManager.IMPORTANCE_DEFAULT);

            notificationChannel.setDescription("lifecycle events");

            notificationChannel.setShowBadge(true);

            NotificationManager notificationManager=getSystemService(NotificationManager.class);

            if (notificationManager!=null){
                notificationManager.createNotificationChannel(notificationChannel);
            }
        }
        NotificationManagerCompat notificationManagerCompat=NotificationManagerCompat.from(this);
        NotificationCompat.Builder notificationBuilder= new NotificationCompat.Builder(this
        ,CHANNEL_ID).setContentTitle(eventName+ " " +activityName ).setContentText(getPackageName()).setAutoCancel(true).setSmallIcon(R.mipmap.ic_launcher);
        notificationManagerCompat.notify((int) System.currentTimeMillis(),notificationBuilder.build());

    }
}