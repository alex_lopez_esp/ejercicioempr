package dam.android.alex.practica4;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.text.DecimalFormat;

public class MainActivity extends LogActivity {
    private  static final String DEBUG_TAG="LogsAndroid-1";
    private static final String KEY_ERROR="errorkey";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setUI();

    }
    private  void setUI(){
        EditText etPulgada=findViewById(R.id.editTextNumber);
        EditText etResultado=findViewById(R.id.editTextNumberDecimal);
        Button buttonConverser =findViewById(R.id.button);
        TextView textoError=findViewById(R.id.mostrarError);
        buttonConverser.setOnClickListener(v ->{
            try {
                textoError.setText("");
                etResultado.setText(convertirAPulgadas(etPulgada.getText().toString()));//Todo Ex1 Two-way conversion cm <-> inch
                etPulgada.setText("");
                Log.i(DEBUG_TAG,"Boton de convertir a Pulgadas");//Todo Ex3 Mostrar en Log boton que se ha pulsado
            }catch (Exception e){
                textoError.setVisibility(View.VISIBLE);
                textoError.setText(e.getMessage());//Todo Ex2 Mostrar Errores
                Log.e("LogsConversor",e.getMessage());
            }
        });
        Button buttonConverser2=findViewById(R.id.button2); // Creamos otro boton
        buttonConverser2.setOnClickListener(v->{
            try {
                textoError.setText("");
                etResultado.setText(convertirAMetros(etPulgada.getText().toString()));// Todo Ex1 Two-way conversion inch <-> cm
                etPulgada.setText("");
                Log.i(DEBUG_TAG,"Boton de convertir a Metros");//Todo Ex3 Mostrar en Log boton que se ha pulsado
            }catch (Exception e){
                textoError.setVisibility(View.VISIBLE);
                textoError.setText(e.getMessage());//Todo Ex2 Mostrar Errores
                Log.e("LogsConversor",e.getMessage());
            }
        });

    }
    private String convertirAPulgadas(String pulgadaText) throws Exception {
        if (Double.parseDouble(pulgadaText)<1){ // En el caso de que el valor sea sea menos que 1 lanza excepcion
            throw new Exception("Sólo números>=1");//Todo Ex4 Mostrar en Log boton que se ha pulsado
        }
        double pulgadaValue=Double.parseDouble(pulgadaText)*2.54; // hacemos el calculo
        return String.valueOf(pulgadaValue); // devolvemos el resultado del calculo
    }
    private String convertirAMetros(String pulgadasText) throws Exception {
        if (Double.parseDouble(pulgadasText)<1){// En el caso de que el valor sea sea menos que 1 lanza excepcion
            throw new Exception("Sólo números>=1");//Todo Ex4 Mostrar en Log boton que se ha pulsado
        }
        double metros=Double.parseDouble(pulgadasText)/2.54;//Pasamos el String a un double
        DecimalFormat df= new DecimalFormat(); //le damos formateo de 2 decimales
        df.setMaximumFractionDigits(2); // establecemos que queremos solo 2 decimales
        return (df.format(metros)); // devolvemos ya formateados
    }

    @Override
    protected void onSaveInstanceState( Bundle outState) {
        TextView textoError=findViewById(R.id.mostrarError);
        outState.putString(KEY_ERROR,textoError.getText().toString());
        super.onSaveInstanceState(outState);

        Log.i(DEBUG_TAG,"on save InstanceState");
    }

    @Override
    protected void onRestoreInstanceState( Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        Log.i(DEBUG_TAG,"on restore InstanceState");

        TextView textError=findViewById(R.id.mostrarError);
        textError.setVisibility(View.VISIBLE);
        textError.setText(savedInstanceState.getString(KEY_ERROR));


    }
}